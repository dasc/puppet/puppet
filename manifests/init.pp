class puppet (
    String $package_name,
    String $service_name,
    String $conflocation,
    String $initconflocation,
    String $initconfensure,
    String $version,
    String $status,
    String $ssldir,
    String $user,
    String $group,
    Boolean $enabled,
    String $environment,
    String $server,
    String $smtpserver,
    Boolean $splay,
    Integer $splaylimit,
    Integer $runinterval,
    Boolean $archive_files,
    String $archive_file_server,
) {
    package {'puppet':
        name   => $package_name,
	ensure => $version,
	notify => Service["${service_name}"],
    }

    service {"${service_name}":
        ensure => $status,
	enable => $enabled,
	subscribe => Package['puppet'],
    }

    file {'puppet.conf':
	path => $conflocation,
	ensure => file,
	owner => $user,
	group => $group,
	mode => '0644',
        require => Package['puppet'],
    }

#	content => epp("puppet$conflocation.epp", {
#	    'environment' => $environment,
#	    'server' => $server,
#	    'smtpserver' => $smtpserver,
#	    'splay' => $splay,
#	    'splaylimit' => $splaylimit,
#	    'runinterval' => $runinterval,
#	    'ssldir' => $ssldir,
#	}),

    $puppet_agent_settings = {
        'server' => $server,
        'environment' => $environment,
        'splay' => $splay,
        'splaylimit' => $splaylimit,
        'runinterval' => $runinterval,
        'smtpserver' => $smtpserver,
        'archive_files' => $archive_files,
        'archive_file_server' => $archive_file_server,
    }

    $puppet_agent_settings.each |$name, $value| {
        ini_setting {"puppet_config_${name}":
            ensure => present,
            section => 'agent',
            setting => "${name}",
            value => "${value}",
            path => $conflocation,
            require => File["${conflocation}"],
        }
    }

    file {'puppet_initscript_conf':
        path => $initconflocation,
	ensure => $initconfensure,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('puppet/etc/default/puppet.epp', {
	    'enabled' => $enabled,
	}),
    }

    # This is required so that nagios can stat the state.yaml file
    # to determine if it has been updated (eg puppet is running)
    file { '/var/lib/puppet':
        ensure => directory,
        mode => '0751',
    }
}
